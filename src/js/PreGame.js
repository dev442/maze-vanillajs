import Phaser from 'phaser';
import config from 'visual-config-exposer';

class PreGame extends Phaser.Scene {
  constructor() {
    super('bootGame');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  preload() {
    // LOAD ALL ASSETS HERE
    document.body.style.background="url("+config.preGameScreen.GameBackgroundImage+")"
        this.load.image('background',config.preGameScreen.BackgroundImage)
        this.load.audio('backmusic',config.settings.Music)
        this.load.image('car',config.settings.ObjectImage)
        this.load.image('road','assets/roadlayout.png')
        this.load.image('finish',config.settings.DestinationImage)
        this.load.image('finish2',config.settings.DestinationImage)
        this.load.image('finish3',config.settings.DestinationImage)
        this.load.image('direction', './assets/direction.png')
        this.load.image('direction2', './assets/direction.png')
      //  this.load.image('points','./assets/star.png')   
      // this.load.image('points1', './assets/star.png')  
        this.load.image('road2','./assets/road.png')
        this.load.image('star','./assets/star.png')
        this.load.image('road3','./assets/road.png')
        this.load.image('roa','./assets/road.png')
        this.load.image('logo',config.preGameScreen.GameLogo)
      //  this.load.image('point3','./assets/star.png') 
        this.load.image('house1','./assets/house1.png')
        this.load.image('start','./assets/start.png')
        this.load.image('grass1','./assets/grass1.png')
        this.load.image('grass2','./assets/grass2.png')
        this.load.image('building','./assets/builiding2.png')

        this.load.html('form', '../assets/form.html');
       
  }

  create() {
    // ALL THIS CODE IS FOR RESPONSIVENESS AND RESIZING
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    //
    // ADD GAME CODE BELOW
    //..
    //..
    let bg=this.add.sprite(0,0,'background')
    bg.setOrigin(0,0).setDisplaySize(740,900)

    const logo = this.add.image(350, 300, 'logo').setScale(0.25);

    const title = this.add.text(
      this.GAME_WIDTH / 3,
      this.GAME_HEIGHT / 10,
      'Maze Runner',
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: config.settings.textColor,
      }
    );
    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.15,
      this.GAME_HEIGHT / 2,
      'Play',
      {
        fontSize: '32px',
        fill: config.settings.textColor,
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 2 + 100,
      'LeaderBoards',
      {
        fontSize: '32px',
        fill: config.settings.textColor,
      }
    );


// already coded
    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();


    playBtn.on('pointerdown', () => {
  //    this.scene.start('playGame'); // **** This is the line used for switching scenes
    });

    playBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('instructions');
    });

    leaderBoardBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
      this.scene.start('leaderboard');
    });

    leaderBoardBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });

    this.scale.on('resize', this.resize, this);
  }

  // BELOW FUNCTIONS ARE USED FOR RESIZING

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
   // this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

 getZoom() {
    return this.cameras.main.zoom;
  }
}

export default PreGame;
