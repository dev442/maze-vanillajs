import Phaser from 'phaser';

class PostGame extends Phaser.Scene {
  constructor() {
    super('endGame');
    this.GAME_WIDTH = 300; //740
    this.GAME_HEIGHT = 900; //900
    this.scores = [];
  }

  init(data) {
    console.log('init', data);
  }
  
  create(data) {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }
    


    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.backgroundScene = this.add
      .image(0, 0, 'background')
      .setOrigin(0, 0)
      .setScale(5);
      
    this.backgroundScene.depth = 0;

    const title = this.add.text(
      this.GAME_WIDTH / 2.5,
      this.GAME_HEIGHT / 10,
      `${data.message}`,
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: '#000',
      }
    );

    const scoreText = this.add.text(
      this.GAME_WIDTH / 2.6,
      this.GAME_HEIGHT / 6,
      `Your Score is ${data.score}`,
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: '#000',
      }
    );

    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.6,
      this.GAME_HEIGHT / 2,
      'Play Again',
      {
        fontSize: '32px',
        fontStyle:'bolder',
        fill: '#000',
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.8,
      this.GAME_HEIGHT / 2 + 100,
      'Submit Score',
      {
        fontSize: '32px',
        fontStyle:'bolder',
        fill: '#000',
      }
    );

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();

    playBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width;
      hoverImage.y = playBtn.y + 10;
    });
    playBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    playBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('playGame', { score: 0 });
      
    });

    leaderBoardBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = leaderBoardBtn.x - leaderBoardBtn.width / 2.5;
      hoverImage.y = leaderBoardBtn.y + 10;
    });
    leaderBoardBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    leaderBoardBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('leaderBoardPost', { score: data.score });
    });

    //this.updateCamera();
    this.scale.on('resize', this.resize, this);
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
    3;
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default PostGame;
