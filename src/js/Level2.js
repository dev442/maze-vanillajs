import Phaser from 'phaser';
import config from 'visual-config-exposer';

let cursors, start, roa, road2, car, grass1, finish, grass2
let backmusic, road3, direction, direction2, house1, building
let level='Medium'
let win=2
let counts=1
/*
let hello=1
let counter = 0;*/
let score = 20,
  scoreText,
  timeText;

class Game extends Phaser.Scene {
  constructor() {
    super('Level2');
    this.GAME_WIDTH = 650;
    this.GAME_HEIGHT = 640;
  }
  init(data) {
    console.log('init', data);
  }
  
  create(data) {
    // ALL THIS CODE IS FOR RESPONSIVENESS AND RESIZING
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;
    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
        this.sizer = new Phaser.Structs.Size(
          this.GAME_WIDTH=600,
          this.GAME_HEIGHT=1000,
          Phaser.Structs.Size.ENVELOP,
          this.parent
        );
      } else if (window.innerWidth >= 800) {
        this.sizer = new Phaser.Structs.Size(
          this.GAME_WIDTH,
          this.GAME_HEIGHT,
          Phaser.Structs.Size.FIT,
          this.parent
        );
      }
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    //
    // ADD GAME CODE BELOW
    //..
    //..
    this.backgroundScene = this.add
    .image(0,0, 'background')
    .setOrigin(0,0)
    .setDisplaySize(740,900)

   






    car = this.physics.add.image(20,40,'car').setScale(0.7)
//.setCollideWorldBounds(true);
    car.body.allowGravity = false;
   car.setDisplaySize(50,25)
   car.depth=2;
  
 
  
  // points.setPosition(50,50)
   
   car.setPosition(50,10)
   car.setAngle(90)
   
   direction = this.physics.add.sprite(70,40,'direction').setDisplaySize(20,20)
   direction.body.allowGravity = false;
   direction2 = this.physics.add.sprite(70,40,'direction2').setDisplaySize(20,20)
   direction2.body.allowGravity = false;
   direction.depth=1;
   direction2.depth=1;
   direction.visible=true
   direction2.visible=false
   direction.setPosition(50,50).setAngle(90)
   finish = this.physics.add.sprite(460,225,'finish').setOrigin(0,0).setScale(0.2)
   finish.body.allowGravity = false;
   finish.setDisplaySize(150,100)
   house1 = this.physics.add.sprite(265,150,'house1').setScale(0.18).setAngle()
   house1.body.allowGravity = false;
   start= this.physics.add.sprite(50,100,'start').setScale(0.15).setPosition(150,50)
   start.body.allowGravity = false;
   grass1=this.physics.add.sprite(450,220,'grass1').setScale(0.1,0.07)
   grass1.body.allowGravity = false;

   grass1.setPosition(270,210)
   house1.setPosition(350,50)

    //road = this.add.image(0,10,'road').setOrigin(0,0).setScale(0.6,0.7)
    road2=this.physics.add.sprite(50,80,'road2').setScale(0.5, 0.5) 
    road2.body.allowGravity=false;
    building=this.physics.add.sprite(550,140,'building').setDisplaySize(100,100)
    building.body.allowGravity=false

  /*  finish2 = gameScreen.physics.add.sprite(550, 260,'finish2').setScale(0.2) 
    finish2.setDisplaySize(150,100)*/
         grass2=this.physics.add.sprite(40,220,'grass2').setScale(0.01)
         grass2.setDisplaySize(70,80)
         grass2.body.allowGravity=false
    backmusic=this.sound.add('backmusic')
    //backmusic.play()
    for(var i=1;i<3;i++){
        road3=this.physics.add.sprite(280,i*140,'road3').setScale(0.5, 1.2).setAngle(90)  
        road3.body.allowGravity=false
      
       }
       road3.setAngle(90)
       roa = this.add.group()
       roa.enableBody=true;

      
           let roal = roa.create(150, 210, 'roa')
           
           roal.setScale(0.5,0.3)
           let short3 = roa.create(400, 210, 'roa')
           
           short3.setScale(0.5,0.3)
           let short2 = roa.create(250,50, 'roa')
            short2.setScale(0.5,0.4)
           
          
          
   cursors = this.input.keyboard.createCursorKeys()   
   this.physics.add.overlap(car, finish, add,null)


   scoreText = this.add.text(250, 400, 'score:' +`${data.score}`, {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  timeText = this.add.text(200, 450,  `Time Left: ${config.settings.timer}`, {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  

  if (config.settings.enableTimer) {
    this.timeLeft = config.settings.timer;
    this.gameTimer = this.time.addEvent({
      delay: 1000,
      callback: function () {
        this.timeLeft--;
        timeText.setText(`Time Left : ${this.timeLeft}`);
        if (config.settings.enableTimer) {
          if (score >= config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else if (score < config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        } else {
          if (score >= config.settings.winScore) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        }
      },
      callbackScope: this,
      loop: true,
    });
  }

 function add(){
    //setNumber(60)
    
    if(win==1){
       // score=score+20
      
       showScore()
        road.destroy()
        finish.destroy()
      //  this.scene.start(endGame')
        
    }    
    
}

function showScore(data) {
  score=data.score+20
  scoreText.setText('score:'+`${data.score}`);
  
} 
  }
 
  update() {
    if (!config.settings.enableTimer) {
      if (score >= config.settings.winScore) {
        this.scene.start('endGame', { score, message: 'You Win' });
      } else {
        this.scene.start('endGame', { score, message: 'You Lose' });
      }
    }
    
      if(level=='Medium'){
    
        setTimeout(() => {
            
            if(car.x>=finish.x){
                this.scene.start('Level3',{score: score+20})
              }
        
            if(cursors.down.isDown && counts==1){
                counts=10
              hideDirection()
                car.setAngle(90)
                car.body.velocity.y=75
                setTimeout(()=> {
                    car.body.velocity.y=0
                    car.setAngle(0)
                car.body.velocity.x=65
                setTimeout(() => {
                 car.body.velocity.x=0
                 direction.visible=true
                 direction2.visible=true
                 direction.setPosition(190,140).setAngle(0)
                 direction2.setPosition(150,170).setAngle(90)
                 counts=2
             }, 1500)
             
                },1700)
             
                }
     
                if(cursors.left.isDown && counts==2){
                    hideDirection()
                 counts=10
                 car.setAngle(180)
                 car.body.velocity.x=-60
                 setTimeout(() => {
                     car.body.velocity.x=0
                     car.setAngle(270)
                     car.body.velocity.y=-75
                     setTimeout(() => {
                         car.body.velocity.y=0
                         counts=1
                     }, 1800);
                     
                 }, 1600);   
         
                
                }
             if(cursors.down.isDown && counts==2) {
                 counts=10
                 hideDirection()
                 car.setAngle(90)
                 car.body.velocity.y=70
                 setTimeout(() => {
                     car.body.velocity.y=0
                     car.setAngle(0)
                     car.body.velocity.x=100
                    setTimeout(() => {
                        car.body.velocity.x=0
                        direction.visible=true
                        direction2.visible=true
                        direction.setPosition(440,280).setAngle(0)
                        direction2.setPosition(400,245).setAngle(270)
                        counts=3
                    }, 2500)
                   
                 }, 2000)
                
             }
     
              if(cursors.left.isDown && counts==3) {
                 counts=10
                  hideDirection()
                 car.setAngle(180)
                 car.body.velocity.x=-100
                 setTimeout(() => {
                     car.body.velocity.x=0
                     car.setAngle(270)
                     car.body.velocity.y=-75
                    setTimeout(() => {
                        car.body.velocity.y=0
                        showDirection()
                        direction.setPosition(150,140).setAngle(0)
                        direction2.setPosition(120,140).setAngle(180)
                        counts=2
                    }, 2000)
                  
                 }, 2600)
                
             }
     
             if(cursors.up.isDown && counts==3){
                 counts=10
                hideDirection()
                car.setAngle(270)
                 car.body.velocity.y=-85
                 setTimeout(() => {
                     car.body.velocity.y=0
                   
                    direction.setPosition(430,140).setAngle(0)
                    direction2.setPosition(350,140).setAngle(180)
                     showDirection()
                     counts=5
                 }, 1700)
             }
             if(cursors.right.isDown && counts==2){
                 counts=10
                hideDirection()
                car.setAngle(0)
                 car.body.velocity.x=85
                 setTimeout(() => {
                     car.body.velocity.x=0
                   
                    direction.setPosition(300,140).setAngle(0)
                    direction2.setPosition(250,100).setAngle(270)
                     showDirection()
                     counts=4
                 }, 1250)
             }
     
             if(cursors.up.isDown && counts==4){
                 counts=10
                hideDirection()
                car.setAngle(270)
                 car.body.velocity.y=-85
                 setTimeout(() => {
                     car.body.velocity.y=0
                   /* direction.setPosition(300,140).setAngle(0)
                     direction2.setPosition(250,100).setAngle(270)
                      showDirection()*/
             
                     counts=31
                 }, 1550)
             }
     
             /*if(cursors.right.isDown && counts==31){
                 counts=10
                hideDirection()
                car.setAngle(270)
                 car.body.velocity.x=85
                 setTimeout(() => {
                     car.body.velocity.x=0
                     direction.setPosition(300,140).setAngle(180)
                     direction.visible=true
                   
                     counts=41
                             }, 1550)
             }
     
             if(cursors.left.isDown && counts==41){
                 counts=10
                hideDirection()
                car.setAngle(180)
                 car.body.velocity.x=-85
                 setTimeout(() => {
                     car.body.velocity.x=0
                     direction.setPosition(300,140).setAngle(180)
                     direction.visible=true
                   
                     counts=41
                             }, 1550)
             }*/
     
             if(cursors.down.isDown && counts==31){
                 counts=10
                hideDirection()
                car.setAngle(90)
                 car.body.velocity.y=85
                 setTimeout(() => {
                     car.body.velocity.y=0
                   direction2.setPosition(280,140).setAngle(0)
                   direction.setPosition(230,140).setAngle(180)
                   showDirection()
                     counts=4
                 }, 1550)
             }
     
             if(cursors.left.isDown && counts==4){
                 counts=10
                 hideDirection()
                 
                 car.setAngle(180)
                  car.body.velocity.x=-85
                  setTimeout(() => {
                      car.body.velocity.x=0
                      showDirection()
                      direction.setPosition(100,140).setAngle(180)
                 direction2.setPosition(150,180).setAngle(90)
                      counts=2
                  }, 1250)
                 
              }
     
             if(cursors.right.isDown && counts==3){
                 counts=10
                 hideDirection()
                 car.body.velocity.x=75
                 setTimeout(() => {
                    car.body.velocity.x=0 
                 },2000);
              
             }
            
             if(cursors.right.isDown && counts==4) {
                 counts=10
                 hideDirection()
                 car.setAngle(0)
                 car.body.velocity.x=75
                 setTimeout(() => {
                     car.body.velocity.x=0
                     direction.setPosition(450,140).setAngle(0)
                     direction2.setPosition(400,170).setAngle(90)
                     showDirection()
                    
                     counts=5
                 }, 2000)
                
             }
             if(cursors.left.isDown && counts==5) {
                 counts=10
                 hideDirection()
                 car.setAngle(180)
                 car.body.velocity.x=-75
                 setTimeout(() => {
                     car.body.velocity.x=0
                     showDirection()
                     direction.setPosition(210,140).setAngle(180)
                     direction2.setPosition(250,100).setAngle(270)
                     counts=4
                 }, 2000)
                
             }
            
             if(cursors.down.isDown && counts==5) {
                 counts=10
                 hideDirection()
                 car.setAngle(90)
                 car.body.velocity.y=75
                 setTimeout(() => {
                     car.body.velocity.y=0
                     showDirection()
                     direction.setPosition(430,280).setAngle(0)
                    direction2.setPosition(380,280).setAngle(180)
                    counts=6
                 }, 1800)
                
             }
             if(cursors.up.isDown && counts==6) {
                 counts=10
                 hideDirection()
                 car.setAngle(270)
                 car.body.velocity.y=-85
                 setTimeout(() => {
                     car.body.velocity.y=0
                     counts=5
                 }, 1800)
                
             }
     
             if(cursors.right.isDown && counts==5){
                 counts=10
                hideDirection()
                car.setAngle(0)
                 car.body.velocity.x=85
                 setTimeout(() => {
                     car.body.velocity.x=0
                     direction.setPosition(450,140).setAngle(180)
                     direction.visible=true
                   
                     counts=41
                             }, 1550)
             }
     
             if(cursors.left.isDown && counts==41){
                 counts=10
                hideDirection()
                car.setAngle(180)
                 car.body.velocity.x=-85
                 setTimeout(() => {
                     car.body.velocity.x=0
                     direction.setPosition(350,140).setAngle(180)
                     direction2.setPosition(400,180).setAngle(90)
                     showDirection()
                   
                     counts=5
                             }, 1550)
             }
     
     
             if(cursors.right.isDown && counts==6) {
                 counts=10
                 hideDirection()
                 car.setAngle(0)
     
                 car.body.velocity.x=85
                 setTimeout(() => {
                     car.body.velocity.x=0
                    counts=7
                 }, 2500)
            
             }
         }, 1000);
     }
   
  function hideDirection() {
    direction.visible=false;
    direction2.visible=false;
  }
  function showDirection(){
    direction.visible=true
    direction2.visible=true
}
 
}


  // BELOW FUNCTIONS ARE USED FOR RESIZING
  
  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default Game;
