import Phaser from 'phaser';
import config from 'visual-config-exposer';

let cursors, start, roa, road2, car, grass1, finish, grass2
let backmusic, road3, direction, direction2, house1, building
let level='Difficult'
let win=3
let counting=1
/*
let hello=1
let counter = 0;*/
let score = 40,
  scoreText,
  timeText;

class Game extends Phaser.Scene {
  constructor() {
    super('Level3');
    this.GAME_WIDTH = 650;
    this.GAME_HEIGHT = 600;
  }
  
  create(data) {
    // ALL THIS CODE IS FOR RESPONSIVENESS AND RESIZING
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;
    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
        this.sizer = new Phaser.Structs.Size(
          this.GAME_WIDTH=600,
          this.GAME_HEIGHT=1000,
          Phaser.Structs.Size.ENVELOP,
          this.parent
        );
      } else if (window.innerWidth >= 800) {
        this.sizer = new Phaser.Structs.Size(
          this.GAME_WIDTH,
          this.GAME_HEIGHT,
          Phaser.Structs.Size.FIT,
          this.parent
        );
      }
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    //
    // ADD GAME CODE BELOW
    //..
    //..
    this.backgroundScene = this.add
    .image(0,0, 'background')
    .setOrigin(0,0)
    .setDisplaySize(740,900)

   






    car = this.physics.add.image(20,40,'car').setScale(0.7)
//.setCollideWorldBounds(true);
    car.body.allowGravity = false;
   car.setDisplaySize(50,25)
   car.depth=2;

  // points.setPosition(50,50)
   
  car.body.velocity.x=0
 car.setPosition(50,0)
  car.setAngle(90)
   
   direction = this.physics.add.sprite(70,40,'direction').setDisplaySize(20,20)
   direction.body.allowGravity = false;
   direction2 = this.physics.add.sprite(70,40,'direction2').setDisplaySize(20,20)
   direction2.body.allowGravity = false;
   direction.depth=1;
   direction2.depth=1;
   direction.visible=true
   direction2.visible=false
   direction.setPosition(50,50).setAngle(90)
   finish = this.physics.add.sprite(510,225,'finish').setOrigin(0,0).setScale(0.2)
   finish.body.allowGravity = false;
   finish.setPosition(470,240)
   finish.setDisplaySize(150,100)
   house1 = this.physics.add.sprite(265,150,'house1').setScale(0.18)
   house1.body.allowGravity = false;
   start= this.physics.add.sprite(50,100,'start').setScale(0.15).setPosition(130,50)
   start.body.allowGravity = false;
   grass1=this.physics.add.sprite(450,220,'grass1').setScale(0.05)
   grass1.body.allowGravity = false;

   grass1.setPosition(350,220)
   house1.setPosition(550, 150 )


    //road = this.add.image(0,10,'road').setOrigin(0,0).setScale(0.6,0.7)
    road2=this.physics.add.sprite(50,80,'road2').setScale(0.5, 0.5) 
    road2.body.allowGravity=false;
    building=this.physics.add.sprite(550,140,'building').setDisplaySize(100,100)
    building.body.allowGravity=false

  /*  finish2 = gameScreen.physics.add.sprite(550, 260,'finish2').setScale(0.2) 
    finish2.setDisplaySize(150,100)*/
        grass2=this.physics.add.sprite(40,220,'grass2').setScale(0.007)
         grass2.setDisplaySize(70,80)
       
         grass2.setPosition(40,220)
         grass2.body.allowGravity=false
    backmusic=this.sound.add('backmusic')
    //backmusic.play()
    for(var i=1;i<3;i++){
        road3=this.physics.add.sprite(280,i*140,'road3').setScale(0.5, 1.2).setAngle(90)  
        road3.body.allowGravity=false
      
       }
       road3.setAngle(90)
       roa = this.add.group()
       roa.enableBody=true;

      
    //       let roal = roa.create(150, 210, 'roa')
           
          // roal.setScale(0.5,0.3)
    //       let short3 = roa.create(400, 210, 'roa')
           
      //     short3.setScale(0.5,0.3)
        //   let short2 = roa.create(250,50, 'roa')
        //    short2.setScale(0.5,0.4)
           
        let road32=roa.create(300,50,'roa')
        road32.setAngle(90)
        road32.setScale(0.4,0.5)    

        let road33=roa.create(200,50,'roa')
        road33.setScale(0.4)

        let road34=roa.create(400,50,'roa')
        road34.setScale(0.4)

        for(var i =1; i<5; i++){ 
        let road35=roa.create(i*100,210,'roa')
        road35.setScale(0.4,0.3)
     // this.physics.add.overlap(car, finish, null)
        }
          
   cursors = this.input.keyboard.createCursorKeys()   
   this.physics.add.overlap(car, finish, add,null)
   scoreText = this.add.text(250, 400, 'score:'+`${data.score}`, {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  timeText = this.add.text(200, 450, `Time Left: ${config.settings.timer}`, {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  

  if (config.settings.enableTimer) {
    this.timeLeft = config.settings.timer;
    this.gameTimer = this.time.addEvent({
      delay: 1000,
      callback: function () {
        this.timeLeft--;
        timeText.setText(`Time Left : ${this.timeLeft}`);
        if (config.settings.enableTimer) {
          if (score >= config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else if (score < config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        } else {
          if (score >= config.settings.winScore) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        }
      },
      callbackScope: this,
      loop: true,
    });
  }
function callEndScreen() {
    this.scene.start('endgame')
}
 function add(){
    //setNumber(60)
    
    if(win==1){
       // score=score+20
      
       showScore()
        road.destroy()
        finish.destroy()
      //  this.scene.start(endGame')
        
    }    
    
}

function showScore() {
  score=score+20
  scoreText.setText('Score: ' + score);
  
} 
  }
 
  update() {
    if (!config.settings.enableTimer) {
      if (score >= config.settings.winScore) {
        this.scene.start('endGame', { score, message: 'You Win' });
      } else {
        this.scene.start('endGame', { score, message: 'You Lose' });
      }
    }
    
    if(level=='Difficult'){
          setTimeout(() => {
              
         
        if(car.x>=finish.x){
            this.scene.start('endGame', {score: score+20, message:'You Win'})
             
           
         }
        if(cursors.down.isDown && counting==1){
            hideDirection()
            counting=10
            car.setAngle(90)
            car.body.velocity.y=75
                            setTimeout(() => {
                                car.body.velocity.y=0
                                car.setAngle(0)
                                car.body.velocity.x=75
                                setTimeout(() => {
                                    car.body.velocity.x=0
                                    counting=2
                                    showDirection()
                                    direction.setPosition(150,140).setAngle(0)
                                    direction2.setPosition(100,180).setAngle(90)
                    
                                }, 700);
                               
                               
                            }, 1900);                                        
        }


        if(cursors.left.isDown && counting==2){
            counting=10
            hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-75
                            setTimeout(() => {
                                car.body.velocity.x=0
                                car.setAngle(270)
                                car.body.velocity.y=-65
                                setTimeout(() => {
                                    car.body.velocity.y=0
                                    counting=1
                                }, 900);
                                
                               
                            }, 1900); 
        }
    
        //count 2 second turn
        if(cursors.down.isDown && counting==2){
            counting=10
            hideDirection()
            car.setAngle(90)
            car.body.velocity.y=80
            setTimeout(()=>{
                car.body.velocity.y=0
               
                counting=3
                direction.visible=true
                direction.setPosition(140,280).setAngle(0)
            },1700)

        }

        
        if(cursors.up.isDown && counting==3){
            hideDirection()
            counting=10
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(()=>{
                car.body.velocity.y=0
                counting=2
               showDirection()
               direction.setPosition(150,140).setAngle(0)
               direction2.setPosition(50,140).setAngle(180)
    
            },1800)
            
    
        }
        if(cursors.right.isDown && counting==3){
            counting=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(()=>{
                car.body.velocity.x=0
               
                counting=21
                direction2.setPosition(250,280).setAngle(0)
                direction.setPosition(200,240).setAngle(270)
               // direction2.setPosition(200,180).setAngle(90)
                showDirection()
               
            },1300)

        }

        if(cursors.left.isDown && counting==21){
            counting=10
            hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-75
            setTimeout(()=>{
                car.body.velocity.x=0
               car.setAngle(270)
               car.body.velocity.y=-75
               setTimeout(() => {
                   car.body.velocity.y=0
                   counting=2
                  
               direction.setPosition(150,140).setAngle(0)
               direction2.setPosition(50,140).setAngle(180)
               showDirection()
               }, 1300);
                
                
               // direction2.setPosition(200,180).setAngle(90)
               
               
            },1800)

        }

        if(cursors.up.isDown && counting==21){
            
            counting=10
            hideDirection()
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(()=>{
                car.body.velocity.y=0
               
                counting=5
                direction.setPosition(150,140).setAngle(180)
                direction2.setPosition(250,140).setAngle(0)
                showDirection()
               
            },1800)

        }

       

     
        if(cursors.right.isDown && counting==21){
            counting=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(()=>{
                car.body.velocity.x=0
               
                counting=22
                direction2.setPosition(350,280).setAngle(0)
                direction.setPosition(300,240).setAngle(270)
               // direction2.setPosition(200,180).setAngle(90)
                showDirection()
               
            },1300)

        }

        if(cursors.left.isDown && counting==22){
            counting=10
            hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-75
            setTimeout(()=>{
                car.body.velocity.x=0
               
                counting=21
                direction2.setPosition(150,280).setAngle(180)
                direction.setPosition(200,240).setAngle(270)
               // direction2.setPosition(200,180).setAngle(90)
                showDirection()
               
            },1300)

        }

        if(cursors.up.isDown && counting==22){
            
            counting=10
            hideDirection()
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(()=>{
                car.body.velocity.y=0
               
                counting=6
                direction.setPosition(250,140).setAngle(180)
                direction2.setPosition(350,140).setAngle(0)
                showDirection()
               
            },1800)

        }

        
        if(cursors.right.isDown && counting==22){
            counting=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(()=>{
                car.body.velocity.x=0
               
                counting=23
                direction2.setPosition(450,280).setAngle(0)
                direction.setPosition(400,240).setAngle(270)
               // direction2.setPosition(200,180).setAngle(90)
                showDirection()
               
            },1300)

        }

        if(cursors.up.isDown && counting==23){
            
            counting=10
            hideDirection()
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(()=>{
                car.body.velocity.y=0
               
                counting=7
                direction.setPosition(350,140).setAngle(180)
                direction2.setPosition(450,140).setAngle(0)
                showDirection()
               
            },1800)

        }


        if(cursors.left.isDown && counting==23){
            counting=10
            hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-75
            setTimeout(()=>{
                car.body.velocity.x=0
               
                counting=22
                direction2.setPosition(250,280).setAngle(180)
                direction.setPosition(300,240).setAngle(270)
               // direction2.setPosition(200,180).setAngle(90)
                showDirection()
               
            },1300)

        }

        if(cursors.right.isDown && counting==23){
            counting=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(()=>{
                car.body.velocity.x=0
          
            },1300)

        }

      /*  if(cursors.left.isDown && counting==3){
            car.setAngle(90)
            car.body.velocity.x=-75
            setTimeout(()=>{
                car.body.velocity.x=0
                setAngle(270)
                car.body.velocity.y=-75
                setTimeout(() => {
                    car.body.velocity.y=0 
                    counting=2  
                }, 2000);
            
            },1000)
    
        }*/
    
        //for right
        if(cursors.right.isDown && counting==2){
            hideDirection()
            counting=10
            car.body.velocity.x=75
            car.setAngle(0)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=5
                showDirection()
                direction.setPosition(200,90).setAngle(270)
                direction2.setPosition(200,180).setAngle(90)
            }, 1300);
        }

        if(cursors.down.isDown && counting==5){
            hideDirection()
            counting=10
            car.body.velocity.y=75
            car.setAngle(90)
            setTimeout(() => {
                car.body.velocity.y=0
                counting=21
                showDirection()
                direction.setPosition(170,280).setAngle(180)
                direction2.setPosition(230,280).setAngle(0)
            }, 1800);
        }
    
        if(cursors.left.isDown && counting==5){
            hideDirection()
            counting=10
            car.body.velocity.x=-75
            car.setAngle(180)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=2
               
                direction.setPosition(50,140).setAngle(180)
                direction2.setPosition(100,180).setAngle(90)
                showDirection()
            }, 1500);
        }

        if(cursors.right.isDown && counting==5){
            hideDirection()
            counting=10
            car.body.velocity.x=75
            car.setAngle(0)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=6
                direction.setPosition(350, 140).setAngle(0)
                direction2.setPosition(300,180).setAngle(90)
                showDirection()
            }, 1500);
        }


        if(cursors.down.isDown && counting==6){
            hideDirection()
            counting=10
            car.body.velocity.y=75
            car.setAngle(90)
            setTimeout(() => {
                car.body.velocity.y=0
                counting=22
                showDirection()
                direction.setPosition(270,280).setAngle(180)
                direction2.setPosition(330,280).setAngle(0)
            }, 1800);
        }
    

        if(cursors.left.isDown && counting==6){
            hideDirection()
            counting=10

            car.body.velocity.x=-75
            car.setAngle(180)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=5
                direction.setPosition(160,140).setAngle(180)
                direction2.setPosition(200,180).setAngle(90)
                showDirection()
            }, 1500);
        }

       if(cursors.right.isDown && counting==6){
           hideDirection()
           counting=10
            car.body.velocity.x=75
            car.setAngle(0)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=7
                direction.setPosition(450, 140).setAngle(0)
                direction2.setPosition(400,180).setAngle(90)
                showDirection()
            }, 1500);
        }
        if(cursors.down.isDown && counting==7){
            hideDirection()
            counting=10
            car.body.velocity.y=75
            car.setAngle(90)
            setTimeout(() => {
                car.body.velocity.y=0
                counting=23
                showDirection()
                direction.setPosition(370,280).setAngle(180)
                direction2.setPosition(430,280).setAngle(0)
            }, 1800);
        }
        if(cursors.left.isDown && counting==7){
            hideDirection()
            counting=10
             car.body.velocity.x=-75
             car.setAngle(180)
             setTimeout(() => {
                 car.body.velocity.x=0
                 counting=6
                 direction.setPosition(270, 140).setAngle(180)
                 direction2.setPosition(300,180).setAngle(90)
                 showDirection()
             }, 1500);
         }


 if(cursors.right.isDown && counting==7){
     hideDirection()
            car.body.velocity.x=75
            car.setAngle(0)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=8
            }, 1500);
        }

        if(cursors.left.isDown && counting==8){
            
                   car.body.velocity.x=-75
                   car.setAngle(0)
                   setTimeout(() => {
                       car.body.velocity.x=0
                       counting=7
                       direction.setPosition(300, 140).setAngle(180)
                       showDirection
                   }, 1500);
               }
    
        //count=3
    //for right
/*          if(cursors.right.isDown && counting==3){
            car.body.velocity.x=75
            car.setAngle(0)
            setTimeout(() => {
                car.body.velocity.x=0
                counting=4
            }, 1000);
        }
        if(cursors.left.isDown && counting==4){
            car.setAngle(180)
            car.body.velocity.x=-75
            
            setTimeout(() => {
                car.body.velocity.x=0
                counting=3
            }, 1000);
        }
        //for up
        if(cursors.up.isDown && counting==3){
            car.body.velocity.y=-75
            car.setAngle(270)
            setTimeout(() => {
                car.body.velocity.y=0
                counting=5
            }, 2000);
        }
        if(cursors.down.isDown && counting==5){
            car.body.velocity.y=+75
            car.setAngle(90)
            setTimeout(() => {
                car.body.velocity.y=0
                counting=3
            }, 2000);
        }*/
    
        
        //count 4
    }, 1000);
    }
   
  function hideDirection() {
    direction.visible=false;
    direction2.visible=false;
  }
  function showDirection(){
    direction.visible=true
    direction2.visible=true
}
 
}


  // BELOW FUNCTIONS ARE USED FOR RESIZING
  
  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default Game;
