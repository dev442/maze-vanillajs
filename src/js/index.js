import Phaser from 'phaser';

import Game from './Game';
import PreGame from './PreGame';
import PostGame from './PostGame';
import Level2 from './Level2';
import Level3 from './Level3';
import Instructions from './Instructions';
import LeaderBoard from './LeaderBoard';
import PostLeaderBoard from './PostLeaderBoard';


const CONFIG = {
  type: Phaser.AUTO,
  backgroundColor: '#000000',
  parent: 'phaser-example',
  scale: {
    mode: Phaser.Scale.RESIZE,
    width: 640,
    height: 900,
    min: {
      width: 320,
      height: 480,
    },
    max: {
      width: 740,
      height: 900,
    },
  },
  dom: {
    createContainer: true,
  },
  scene: [PreGame, Game, PostGame, Level2, Level3, Instructions, LeaderBoard, PostLeaderBoard],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      // debug: true,
      // debugShowBody: true,
      // debugShowStaticBody: true,
    },
  },
};

let game = new Phaser.Game(CONFIG);
