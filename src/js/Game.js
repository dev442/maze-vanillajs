import Phaser, { Data } from 'phaser';
import config from 'visual-config-exposer';

let cursors, start, road, car, grass1, finish
let backmusic, direction, direction2, house1
let level
let win=1
let count=1

/*
let hello=1
let counter = 0;*/
let score = 0,
  scoreText,
  timeText;

class Game extends Phaser.Scene {
  constructor() {
    super('playGame');
    this.GAME_WIDTH = 650;
    this.GAME_HEIGHT = 640;
  }
  
  create() {
   
    // ALL THIS CODE IS FOR RESPONSIVENESS AND RESIZING
    level='Easy'
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;
    this.parent = new Phaser.Structs.Size(width, height);
     if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH=600,
        this.GAME_HEIGHT=1000,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    //
    // ADD GAME CODE BELOW
    //..
    //..
    this.backgroundScene = this.add
    .image(0,0, 'background')
    .setOrigin(0,0)
    .setDisplaySize(740,900)
    road = this.add.image(0,10,'road').setOrigin(0,0).setScale(0.6,0.7)
    backmusic=this.sound.add('backmusic')
    backmusic.play()
    //backmusic.play()

    car = this.physics.add.image(20,40,'car').setScale(0.7)
//.setCollideWorldBounds(true);
    car.body.allowGravity = false;
   car.setDisplaySize(50,25)

   direction = this.physics.add.sprite(70,40,'direction').setDisplaySize(20,20)
   direction.body.allowGravity = false;
   direction2 = this.physics.add.sprite(70,40,'direction2').setDisplaySize(20,20)
   direction2.body.allowGravity = false;
   finish = this.physics.add.sprite(510,225,'finish').setOrigin(0,0).setScale(0.2)
   finish.body.allowGravity = false;
   finish.setDisplaySize(150,100)
   house1 = this.physics.add.sprite(265,150,'house1').setScale(0.18).setAngle()
   house1.body.allowGravity = false;
   start= this.physics.add.sprite(50,100,'start').setScale(0.15)
   start.body.allowGravity = false;
   grass1=this.physics.add.sprite(450,220,'grass1').setScale(0.1,0.07)
   grass1.body.allowGravity = false;
   cursors = this.input.keyboard.createCursorKeys()   
   this.physics.add.overlap(car, finish, add,null)


   scoreText = this.add.text(250, 400, 'score: 0', {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  timeText = this.add.text(200, 450, `Time Left: ${config.settings.timer}`, {
    fontSize: '32px',
    fontStyle:'bolder',
    fill: config.settings.textColor,
  });
  

  if (config.settings.enableTimer) {
    this.timeLeft = config.settings.timer;
    this.gameTimer = this.time.addEvent({
      delay: 1000,
      callback: function () {
        this.timeLeft--;
        timeText.setText(`Time Left : ${this.timeLeft}`);
        if (config.settings.enableTimer) {
          if (score >= config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else if (score < config.settings.winScore && this.timeLeft <= 0) {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        } else {
          if (score >= config.settings.winScore) {
            this.scene.start('endGame', { score, message: 'You Win' });
          } else {
            this.scene.start('endGame', { score, message: 'You Lose' });
          }
        }
      },
      callbackScope: this,
      loop: true,
    });
  }

 function add(){
    //setNumber(60)
    
    if(win==1){
       // score=score+20
      
       showScore()
        road.destroy()
        finish.destroy()
      //  this.scene.start(endGame')
        
    }    
    
}

function showScore() {
 score=score+20
 // scoreText.setText('Score: ' + score);
  
} 
  }
 
  update(pointer) {
    if (!config.settings.enableTimer) {
      if (score >= config.settings.winScore) {
        this.scene.start('endGame', { score, message: 'You Win' });
      } else {
        this.scene.start('endGame', { score, message: 'You Lose' });
      }
    }

    
    if(level=='Easy'){

if(car.x>=finish.x){
  this.scene.start('Level2',{score: score})
}
      // points.angle+=1
      // points1.angle+=1
      if((cursors.right.isDown && count==1) || (pointer.x>=60 && count==1)){
       console.log(pointer)
          count=10
          direction.setAngle(0)
          direction.visible=false
          direction2.visible=false
          car.setAngle(0)
          car.body.velocity.x=85
          setTimeout(()=> {
              car.body.velocity.x=0
              direction.setPosition(220,40)
              direction2.setPosition(175,70)
              direction.visible=true
              direction2.setAngle(90)
              direction2.visible=true
              
              count=2
          },1800)
          
      }
  
          if(cursors.left.isDown && count==2){
             count=10
         hideDirection()
           car.setAngle(180) 
           car.body.velocity.x=-75
           setTimeout(()=> {
              car.body.velocity.x=0
              if(car.body.velocity.x==0){
                  count=1
              }
             
          },2000)
          
          }
  
         
      if(cursors.down.isDown && count==2){
          count=10
          hideDirection()
          car.setAngle(90)
          car.body.velocity.y=75
          setTimeout(()=> {
               car.body.velocity.y=0
               direction.setPosition(210,280).setAngle(0)
               direction.visible=true
               count=3
            },3200)
          
           }
  
           if(cursors.up.isDown && count==3){
              count=10
               hideDirection()
              car.setAngle(-90)
              car.body.velocity.y=-75
              setTimeout(()=> {
                  car.body.velocity.y=0
                  direction.setPosition(210,40)
              direction2.setPosition(150,40)
              direction.visible=true
              direction2.setAngle(1800)
              direction2.visible=true
                  count=2
              },3200)
             
              }
  
  
          if(cursors.right.isDown && count==2){
              count=10
              hideDirection()
              car.setAngle(0)
              car.body.velocity.x=75
              setTimeout(()=> {
                  car.body.velocity.x=0
                 direction.visible=true
                 direction.setPosition(355,70).setAngle(90)
                  count=4
              },2400)
          
          }
          if(cursors.left.isDown && count==4){
              count=10
         hideDirection()
  
              car.setAngle(180) 
              car.body.velocity.x=-85
              setTimeout(()=> {
                 car.body.velocity.x=0
                 showDirection()
                 direction.setPosition(125,40).setAngle(180)
                 direction2.setPosition(175,70)
                
                 direction2.setAngle(90)
                
                 
                  count=2
             },2100)
            
             }
  
  
             if(cursors.down.isDown && count==4){
              count=10
                 hideDirection()
                 
              car.setAngle(90)
              car.body.velocity.y=75
              setTimeout(()=> {
                  car.body.velocity.y=0
                  direction.visible=true
                  direction2.visible=true
                  direction.setPosition(385,280).setAngle(0)
                 direction2.setPosition(330,280).setAngle(180)
                  count=5
              },3200)
          
          }
  
          if(cursors.up.isDown && count==5){
              count=10
              hideDirection()
              car.setAngle(270)
              car.body.velocity.y=-75
              setTimeout(()=> {
                  car.body.velocity.y=0
                 direction2.visible=true
                  direction2.setPosition(320,40).setAngle(180)
                  count=4
              },3200)
          
          }
         
  
           if(cursors.right.isDown && count==3){
              count=10
              direction.visible=false
            car.setAngle(0)
               car.body.velocity.x=75
               setTimeout(()=> {
               car.body.velocity.x=0
               direction.visible=true
               direction.setPosition(395,280).setAngle(0)
       direction2.setPosition(355,250).setAngle(270)
       direction2.visible=true
               count=5
           },2350)
    }
  
                     if(cursors.left.isDown && count==5){
                      count=10
                      car.setAngle(180)
                      hideDirection()
                         car.body.velocity.x=-75
                         setTimeout(()=> {
                         car.body.velocity.x=0
                         count=3
                     },2400)
                          
                               }
                               if(cursors.right.isDown && count==5){
                                  count=10
                                   hideDirection()
                                  car.setAngle(0)
                                     car.body.velocity.x=75
                                     setTimeout(()=> {
                                     car.body.velocity.x=0
                                     count=6
                                 },2500)
                          }
  }
  function hideDirection() {
    direction.visible=false;
    direction2.visible=false;
  }
  function showDirection(){
    direction.visible=true
    direction2.visible=true
}
 
}


  // BELOW FUNCTIONS ARE USED FOR RESIZING
  
  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default Game;
